'''
Created on 10 mar. 2021

@author: cesar
'''
import tkinter as tk
import math
from lib2to3.pgen2.token import OP

ventana = tk.Tk()
ventana.title("Calculadora")
ventana.geometry("400x235")

entrada = tk.Entry()
entrada.place(x=0,y=0 ,width=400,height=50)
guardado = ""
op = 0

def residuo():
    global op
    global guardado
    guardado = entrada.get()
    op = 3
    entrada.delete(0,'end')
    pass
def cE():
    global op
    op = 0
    guardado = ""
    entrada.delete(0,'end')
    pass
def delete():
    if(entrada.get()!=""):
        x = entrada.get()[:-1]
        entrada.delete(0,'end')
        entrada.insert(tk.END,x)
    pass
def sobreX():
    if(entrada.get()!="0"):
        if(entrada.get()=="" or entrada.get()=="."):
            r = 0.0
        else:
            r = float(entrada.get())  
        entrada.delete(0,'end')
        if(r!=0):
            r = 1/r
            entrada.insert(0,str(r))
        else:
            entrada.insert(0,"Es necesario escribir un numero")
    else:
        entrada.insert(0,"Error matematico: dividir entre 0")
    pass
def cuadradaX():
    if(entrada.get()!=""):
        r = float(entrada.get())
        r= r*r
        entrada.delete(0,'end')
        entrada.insert(0,str(r))      
    pass
def sqrtX():
    if(entrada.get()!=""):
        r = float(entrada.get())
        if(r>0):
            r = math.sqrt(r)
            entrada.delete(0,'end')
            entrada.insert(0,str(r))
        else:
            entrada.delete(0,'end')
            entrada.insert(0,"Error matematico: Raiz de números negativos")
    pass
def dividir():
    global op
    global guardado
    op =2
    guardado = entrada.get()
    entrada.delete(0,'end')
    pass
def multiplicacion():
    global op
    global guardado
    op =4
    guardado = entrada.get()
    entrada.delete(0,'end')
    pass
def resta():
    global op
    global guardado
    op =5
    guardado = entrada.get()
    entrada.delete(0,'end')
    pass
def suma():
    global op
    global guardado
    op = 1
    guardado= entrada.get()
    entrada.delete(0,'end')
    pass




def press1():
    entrada.insert(tk.INSERT,"1")
    pass
def press2():
    entrada.insert(tk.INSERT,"2")
    pass
def press3():
    entrada.insert(tk.INSERT,"3")
    pass
def press4():
    entrada.insert(tk.INSERT,"4")
    pass
def press5():
    entrada.insert(tk.INSERT,"5")
    pass
def press6():
    entrada.insert(tk.INSERT,"6")
    pass
def press7():
    entrada.insert(tk.INSERT,"7")
    pass
def press8():
    entrada.insert(tk.INSERT,"8")
    pass
def press9():
    entrada.insert(tk.INSERT,"9")
    pass
def press0():
    entrada.insert(tk.INSERT,"0")
    pass
def punto():
    if(entrada.get().find(".")==-1):
        entrada.insert(tk.INSERT,".")
    pass
def igual():
    if(op==1):
        n2 = float(entrada.get())
        entrada.delete(0,'end')
        n1 = float(guardado)
        r = n1+n2
        entrada.insert(0, str(r))
    if(op==2):
        n2 = float(entrada.get())
        entrada.delete(0,'end')
        n1 = float(guardado)
        if(n2==0):
            entrada.insert(0, "Error matematico: Dividir entre 0")
        else:
            r = n1/n2
            entrada.insert(0, str(r))
    if(op==3):
        n2 = float(entrada.get())
        entrada.delete(0,'end')
        n1 = float(guardado)
        r = n1%n2
        entrada.insert(0, str(r))
    if(op==4):
        n2 = float(entrada.get())
        entrada.delete(0,'end')
        n1 = float(guardado)
        r = n1*n2
        entrada.insert(0, str(r))
    if(op==5):
        n2 = float(entrada.get())
        entrada.delete(0,'end')
        n1 = float(guardado)
        r = n1-n2
        entrada.insert(0, str(r))
    pass


btnX =tk.Button(ventana, text="", bg='blue')
btnX.place(x=0,y=50,width=400,height=5)

bResiduo = tk.Button(ventana, text="%", command=residuo)
bResiduo.place(x=0,y=55,width=100,height=30)

bCE = tk.Button(ventana, text="CE", command=cE)
bCE.place(x=100,y=55,width=100,height=30)

bDelete = tk.Button(ventana, text="<=", command=delete)
bDelete.place(x=200,y=55,width=200,height=30)

b1entreX = tk.Button(ventana, text="1/X", command=sobreX)
b1entreX.place(x=0,y=85,width=100,height=30)

bCuadrado = tk.Button(ventana, text="X^2", command=cuadradaX)
bCuadrado.place(x=100,y=85,width=100,height=30)

bSqrt = tk.Button(ventana, text="√X",  command=sqrtX)
bSqrt.place(x=200,y=85,width=100,height=30)

bDividir = tk.Button(ventana, text="÷", command=dividir)
bDividir.place(x=300,y=85,width=100,height=30)

b7 = tk.Button(ventana, text="7", command=press7)
b7.place(x=0,y=115,width=100,height=30)

b8 = tk.Button(ventana, text="8", command=press8)
b8.place(x=100,y=115,width=100,height=30)

b9 = tk.Button(ventana, text="9", command=press9)
b9.place(x=200,y=115,width=100,height=30)

bMulti = tk.Button(ventana, text="x", command=multiplicacion)
bMulti.place(x=300,y=115,width=100,height=30)

b4 = tk.Button(ventana, text="4",  command=press4)
b4.place(x=0,y=145,width=100,height=30)

b5 = tk.Button(ventana, text="5", command=press5)
b5.place(x=100,y=145,width=100,height=30)

b6 = tk.Button(ventana, text="6",  command=press6)
b6.place(x=200,y=145,width=100,height=30)

bResta = tk.Button(ventana, text="-",  command=resta)
bResta.place(x=300,y=145,width=100,height=30)

b1 = tk.Button(ventana, text="1",  command=press1)
b1.place(x=0,y=175,width=100,height=30)

b2 = tk.Button(ventana, text="2", command=press2)
b2.place(x=100,y=175,width=100,height=30)

b3 = tk.Button(ventana, text="3",  command=press3)
b3.place(x=200,y=175,width=100,height=30)

bSuma = tk.Button(ventana, text="+", command=suma)
bSuma.place(x=300,y=175,width=100,height=30)

b0 = tk.Button(ventana, text="0",  command=press0)
b0.place(x=0,y=205,width=150,height=30)

bPunto = tk.Button(ventana, text=".", command=punto)
bPunto.place(x=150,y=205,width=100,height=30)

bIgual = tk.Button(ventana, text="=",  command=igual)
bIgual.place(x=250,y=205,width=150,height=30)


ventana.mainloop()